const jwt = require('jsonwebtoken');

//password can be hidden in .env file
const dotenv = require('dotenv');
dotenv.config();
const secret = process.env.SECRET


module.exports.createAccessToken =(user)=>{
	console.log(user);

	const data ={
		id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}
	return jwt.sign(data,secret,{})

}

module.exports.verify = (req, res, next) => {
	console.log(req.headers.authorization)

	let token = req.headers.authorization;
	if(typeof token === "undefined"){
		return res.send({ auth: "Failed. No token" });
	} else {
		console.log(token);
		token = token.slice(7, token.length)
		jwt.verify(token, secret, function(err, decodedToken) {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				console.log(decodedToken); //contains the data from our token
				req.user = decodedToken
				next();
			}
		})


	}
}


module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin) {
		next();
	}else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}


