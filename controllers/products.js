const Product = require('../models/Product');


module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error ) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	}).catch(error => error)
}

//active 
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	}).catch(error => error)
}


//Retrieving a specic course
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}

//[update]
module.exports.updateProduct = (prodId, data) => {
	//specify the fields/properties of the document to be updated
	let updatedProd = {
		name: data.name,
		description: data.description,
		price: data.price,
		//isActive:data.isActive
	}

	//findByIdAndUpdate(document Id, updatesToBeApplied)
	return Product.findByIdAndUpdate(prodId, updatedProd).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}



//Archiving 
module.exports.archiveProduct = (prodId) => {
	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(prodId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}

































