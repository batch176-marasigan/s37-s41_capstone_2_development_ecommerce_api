//===========ecom controller ======
//[SECTION] Dependencies and Modules
  const User = require('../models/User'); 
  const Product = require('../models/Product'); 
  const bcrypt = require('bcrypt'); 
  const dotenv = require('dotenv');
  const auth = require('../auth.js');

//[SECTION] Environment Variables Setup
	dotenv.config();
	const asin =parseInt(process.env.SALT);
	
//[SECTION] Functionalities [CREATE]
	module.exports.register = (userData) => {
        let email = userData.email;
        let passW = userData.password;
        let newUser = new User({
         	email: email,
        	password: bcrypt.hashSync(passW,asin), 
          }); 
   	return newUser.save().then((user, err) => {
			if (user) {
				return user; 	
			} else {
				return 'Failed to Register account'; 
			}; 
		}); 
	}; 

//USER Authentication
module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection that matches the search criteria
		return User.findOne({ email: data.email }).then(result => {
				if(result == null) {
						return false;
				} else {
						const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
						if(isPasswordCorrect){
								return { accessToken: auth.createAccessToken(result.toObject()) }
						}else {
							return false;
						}

				}
		})
}


//[SECTION] Functionalities [RETRIEVE]
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
			result.password = '';
			return result;
	})
}



//-----this is pending***********    CHECKOUT CART NON ADMIN-------
module.exports.order = async (req, res) => {
		console.log(req.user.id);//the user's id from the decoded token after verify()
		console.log(req.body.prodId); //the course ID from our request body

		if(req.user.isAdmin) {
				return res.send({ message: "Action Forbidden"})
		}

		let isUserUpdated = await User.findById(req.user.id).then( user => {
							//add the courseId in an object and push that object into user's enrollments array:

							let newOrder = {
									prodId: req.body.prodId
							}
							user.order.push(newOrder);
							return user.save().then(user => true).catch(err => err.message)
							if(isUserUpdated !== true) {
									return res.send({ message: isUserUpdated })
						}
		})


		//Find the course Id that we will need to push to our enrollee
		let isProductUpdated = await Product.findById(req.body.prodId).then(course => {
					//create an object which will be push to enrollees array/field
					let order = {
							userId: req.user.id
					}

					product.orders.push(order);

					//save the course document
					return product.save().then(product => true).catch(err => err.message)


					if(isProductUpdated !== true) {
							return res.send({ message: isProductUpdated })
					}

		})

		if(isUserUpdated && isProductUpdated) {
				return res.send({ message: "Cart added Successfully" })
		}

}



//Get user's orders
module.exports.getOrders = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(err => res.send(err))
}


//[SECTION] Functionalities [UPDATE] -user as ADMIN (FOR ADMIN ACCESS ONLY)
module.exports.updateUser = (userId, data) => {
	//specify the fields/properties of the document to be updated
	let updatedUser = {
		email: data.email,
		isAdmin: data.isAdmin
		
	}

	//findByIdAndUpdate(document Id, updatesToBeApplied)
	return User.findByIdAndUpdate(userId, updatedUser).then((isUserUpdated, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}

