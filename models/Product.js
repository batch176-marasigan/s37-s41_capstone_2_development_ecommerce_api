// [SECTION] Dependencies and Modules
 const mongoose = require('mongoose')

// [SECTION] Schema/Blueprint
 const productSchema = new mongoose.Schema({
	name:{
		type:String,
		required: [true,'is Required']
	},	
	description:{
		type:String,
		required: [true, 'is Required']
	},
	price:{
		type:Number,
		required:[true,'Price is Required']
	},
	isActive:{
		type:Boolean,
		default:true
	},
	createdOn:{
		type:Date,
		default:new Date()
	},
	orders:[
		{
			orderId:{
				type:String,
				required: [true,'id is Required']
			}
		}
	]
});

// [SECTION] Model
 //module.exports.mongoose.model('Course', courseSchema);
 module.exports = mongoose.model('Product', productSchema);


// [SECTION]

// [SECTION]

// [SECTION]