//[SECTION] Modules and Dependencies
	const mongoose = require('mongoose');
	const userSchema = new	mongoose.Schema({
		email:{
			type:String,
			required: [true, 'Email is Required']		
		},
		password:{
			type:String,
			required: [true, 'Password is Required']		
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		
		orders:[
		{
				prodId: {
					type: String,
					required: [true,'Product ID is Required']
					},
				productName: {
					type: String,
					required: [true,'Is Required']
					},
				quantity: {
					type: Number,
					required: [true,'is Required']
					},
				totalAmount:{
					type: Number,
					required: [true,'is Required']
					},		
				purchasedOn:{
					type: Date,
					default: new Date()
					} 
		}
	]
});


//[SECTION] Model
	module.exports = mongoose.model('User',userSchema);
