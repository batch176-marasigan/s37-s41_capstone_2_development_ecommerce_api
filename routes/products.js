const express = require('express');
const route = express.Router();
const ProductController = require('../controllers/products')
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

//Route for Creating  
route.post('/create', verify, verifyAdmin, (req, res) => {
	console.log(req.body);
	ProductController.addProduct(req.body).then(result => res.send(result))
})


//Retrieve all   -required token from admin
route.get('/all', verify, verifyAdmin, (req, res) => {
	ProductController.getAllCourses().then(result => res.send(result));
})

//Retrieve all ACTIVE     -- non admin users
route.get('/active', (req, res) => {
	ProductController.getAllActive().then(result => res.send(result));
})


//Retrieving a SPECIFIC 
route.get('/:prodId', (req, res) => {
	console.log(req.params.prodId)
	ProductController.getProduct(req.params.prodId).then(result => res.send(result));
})


//Route for UPDATING a course
route.put('/:prodId', verify, verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params.prodId, req.body).then(result => res.send(result))
})


//Archiving a course
route.put('/:prodId/archive', verify, verifyAdmin, (req, res) => {
	ProductController.archiveProduct(req.params.prodId).then(result => res.send(result));
})


module.exports = route;