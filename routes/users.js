//[SECTION] Dependencies and Modules
	const exp = require('express'); //needed library to implement a new route
	const UserController = require('../controllers/users');
	const auth = require('../auth');

	const { verify, verifyAdmin } = auth;


//[SECTION] Routing Component
	const route = exp.Router(); 

//[SECTION] Routes- POST
	route.post('/register', (req, res) => {
		console.log(req.body);  //display in gitbash
		let userData = req.body;
		UserController.register(userData).then(outcome=>{
			res.send(outcome);
		});
	}); 

//[SECTION ] Routes for user authentication(Login)
route.post('/login', (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
})


//[SECTION] Routes- GET the user's details
route.get('/details', auth.verify, (req, res) => {
	UserController.getProfile(req.user.id).then(result => res.send(result));
})


//***********pending work in progress [SET USER AS ADMIN]*************

//Route for UPDATING a USER AS ADMIN - FOR ADMIN ONLY
route.put('/:updateUser', verify, verifyAdmin, (req, res) => {
	console.log(req.body);
	UserController.updateUser(req.params.userId, req.body).then(result => res.send(result))
})



//Enroll our registered Users
//only the verified user can enroll in a course
//route.post('/enroll', auth.verify, UserController.enroll);




//NON ADMIN USER CHEOKOUT
//
route.post('/order_nonAdmin', auth.verify, UserController.order);



// //Get logged user's enrollments
 route.get('/getOrders', auth.verify, UserController.getOrders);




module.exports = route; 